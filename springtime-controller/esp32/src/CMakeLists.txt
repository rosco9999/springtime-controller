idf_component_register(
        SRCS "main.c" "cmd_system.c" "command_task.cc" "Controller.cc" "controller_task.cc" "Parameters.cc" "Pump.cc" "StateMachine.cc"
        INCLUDE_DIRS "."
        REQUIRES console spi_flash nvs_flash )
