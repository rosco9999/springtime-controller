/*
 * Main program for the ESP32-based Project Springtime controller.
 */

#include <stdio.h>
#include "command_task.hh"
#include "controller_task.hh"
#include "esp_system.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * Initialize non-volatile storage
 */
static void initialize_nvs(
        void )
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        printf( "Erasing nvs" );
        ESP_ERROR_CHECK( nvs_flash_erase() );

        printf( "Initializing nvs" );
        err = nvs_flash_init();
    }

    printf( "Done initializing nvs" );
    ESP_ERROR_CHECK(err);
}

void app_main(
        void )
{
    // initialize non-volatile storage
    initialize_nvs();

    xTaskCreate( command_task,    "command task",    1024 * 10, NULL, 10, NULL );
    xTaskCreate( controller_task, "controller task", 1024 * 10, NULL, 10, NULL );
}
