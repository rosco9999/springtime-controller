/**
 * The central controller of the behavior of the system
 */

#include "Controller.hh"
#include "Parameters.hh"
#include "StateMachine.hh"

/*
 * Proxy to the Pump
 */

Pump thePump( 13 ); // Pin 13

// States in the State machine

/**
 * The pump is supposed to be on.
 */
class PumpActiveOnState :
        public State
{
public:
    PumpActiveOnState(
            const char * name )
        :
            State( name )
    {}

    virtual void enter(
            const ExecutionContext * contextP ) override
    {
        const Parameters * parsP = contextP->getParametersP();
        int                duty = parsP->get( Controller::PAR_PUMP_DUTY );

        thePump.set( duty );
    }

    virtual void leave(
            const ExecutionContext * contextP ) override
    {
        thePump.set( 0 );
    }
};

/**
 * The pump is supposed to be off.
 */
class PumpActiveOffState :
        public State
{
public:
    PumpActiveOffState(
            const char * name )
        :
            State( name )
    {}
};


class PumpActiveState :
        public HierarchicalState
{
private:
    PumpActiveOnState  theOn;
    PumpActiveOffState theOff;
    long               theLastPumpStarted;

public:
    PumpActiveState()
        :
            HierarchicalState( "PumpActiveState", &theOn ),
            theOn( "PumpActiveOnState" ),
            theOff( "PumpActiveOffState" ),
            theLastPumpStarted( -1 )
    {}

    virtual void execute(
            const ExecutionContext * contextP ) override
    {
        HierarchicalState::execute( contextP );

        const Parameters * parsP = contextP->getParametersP();
        long               now   = contextP->getCurrentTime();

        if( theLastPumpStarted < 0 ) {
            theLastPumpStarted = contextP->getCurrentTime();
        }

        if( theChildStateP == &theOn ) {
            // pump running

            int durationOn = parsP->get( Controller::PAR_PUMP_ON );

            if( now > theLastPumpStarted + durationOn ) {
                // turn off
                transitionTo( &theOff, contextP );
            }

        } else {
            // pump off

            int durationCycle = parsP->get( Controller::PAR_PUMP_PERIOD_DAY );

            if( now > theLastPumpStarted + durationCycle) {
                // turn on
                transitionTo( &theOn, contextP );
                theLastPumpStarted = now;
            }
        }
    }
};

const char * const Controller::PAR_PUMP_DUTY         = "pump-duty";
const char * const Controller::PAR_PUMP_ON           = "pump-on";
const char * const Controller::PAR_PUMP_PERIOD_DAY   = "pump-period-day";


// ----


/*
 * Controller's state machine
 */
static PumpActiveState rootState;
static StateMachine    stateMachine( &rootState );

/*
 * Parameters that can be changed at run-time
 */
static Parameter _pars [] = {
        {
            Controller::PAR_PUMP_DUTY,
            750   /* PWM duty cycle. Out of 1024 (10 bit)*/
        },
        {
            Controller::PAR_PUMP_ON,
            8000     /* Run pump for 8 seconds */
        },
        {
            Controller::PAR_PUMP_PERIOD_DAY,
            300000   /* During the day, run pump every 10 min */
        }
};

static Parameters parameters( sizeof( _pars ) / sizeof( _pars[0] ), _pars );

/*
 * Singleton controller instance
 */
Controller CONTROLLER(
        &stateMachine,
        &thePump,
        &parameters );

