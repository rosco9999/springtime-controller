/**
 * The controller task, and everything run from there
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void controller_task();

#ifdef __cplusplus
}
#endif
