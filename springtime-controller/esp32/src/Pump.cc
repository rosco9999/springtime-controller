/*
 * Pump abstraction.
 */


#include "Pump.hh"
#include <stdio.h>

void Pump::init()
{
    ledc_timer_config_t ledc_timer = {
        .speed_mode      = theLedcSpeedMode,
        .duty_resolution = theLedcResolution,
        .timer_num       = theLedcTimer,
        .freq_hz         = theLedcFrequency,
        .clk_cfg         = LEDC_AUTO_CLK,
    };

    ledc_timer_config( &ledc_timer );

    ledc_channel_config_t ledc_channel = {
        .gpio_num   = theGpioPin,
        .speed_mode = theLedcSpeedMode,
        .channel    = theLedcChannel,
        .intr_type  = LEDC_INTR_DISABLE,
        .timer_sel  = theLedcTimer,
        .duty       = 0,
        .hpoint     = 0,
    };

    ledc_channel_config( &ledc_channel );
}

void Pump::set(
        int newValue )
{
    printf( "Setting pump to %d\n", newValue );

    ledc_set_duty(    theLedcSpeedMode, theLedcChannel, newValue );
    ledc_update_duty( theLedcSpeedMode, theLedcChannel ); // flushes the value
}
