/**
 * The task that runs the console command processor and everything related
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void command_task();

#ifdef __cplusplus
}
#endif
